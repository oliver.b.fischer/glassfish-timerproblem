package net.sweblog.timerproblem.servicea;

import net.sweblog.timerproblem.serviceb.ServiceB;

import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import java.util.logging.Level;
import java.util.logging.Logger;

@Singleton
public class ServiceA {
    protected Logger logger = Logger.getLogger(ServiceA.class.getName());

    @EJB
    ServiceB serviceB;

    @Schedule(second = "*/5", minute = "*", hour = "*", persistent = false)
    public void methodOfA() {
        logger.log(Level.INFO, "This is a service A before calling service B.");

        try {
            serviceB.methodToBeCalledByServiceA();
        } catch (RuntimeException e) {

            logger.log(Level.SEVERE, "Caught an exception.", e);
        }

        logger.log(Level.INFO, "This is a service A after calling service B.");
    }
}
