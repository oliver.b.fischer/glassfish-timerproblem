package net.sweblog.timerproblem.serviceb;

import javax.ejb.Singleton;
import java.util.logging.Level;
import java.util.logging.Logger;

@Singleton
public class ServiceB {
    protected Logger logger = Logger.getLogger(ServiceB.class.getName());

    public void methodToBeCalledByServiceA() {

        if (2 > 1) {
            throw new RuntimeException("Peng1");
        }

        logger.log(Level.INFO, "This is service B after throwing an exception?");

    }
}
